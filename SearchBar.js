/**
 * Created by admin on 5/7/15.
 */
var SearchBar = React.createClass({displayName: "SearchBar",
    handleChange: function() {
        this.props.onUserInput(
            //this.refs.filterTextInput.getDOMNode().value,
            //this.refs.inStockOnlyInput.getDOMNode().checked,
            this.refs.isVisiblePr.getDOMNode().checked,
            this.refs.isVisibleODP.getDOMNode().checked,
            this.refs.isVisibleFDP.getDOMNode().checked,
            this.refs.isVisibleOMP.getDOMNode().checked,
            this.refs.isVisibleIVM.getDOMNode().checked,
            this.refs.groupSelected.getDOMNode().value
        );
    },
    roundChange: function(){
        this.props.onUserRoundChange(
            this.refs.roundAcc.getDOMNode().value
        );
    },
    render: function() {
        var style = {
            display: 'inline-block',
            paddingLeft: 3
            };
        var style1 = {
            display: 'inline-block',
            paddingLeft: 15
            };
        return (
React.createElement("form", null, 
                React.createElement("p", {style: style1}, 
                    "Group By:", 
                    React.createElement("select", {
                        name: "groupBy", 
                        ref: "groupSelected", 
                        id: "categorySelect", 
                        onChange: this.handleChange
                    }, 
                        React.createElement("option", {value: "sector"}, "Sector"), 
                        React.createElement("option", {value: "industry"}, "Industry"), 
                        React.createElement("option", {value: "country"}, "Country")
                    )
                ), 
                React.createElement("p", {style: style1}, 
                    "Show columns:", 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visiblePr, 
                        ref: "isVisiblePr", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "Pr"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleODP, 
                        ref: "isVisibleODP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "1D"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleFDP, 
                        ref: "isVisibleFDP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "5D"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleOMP, 
                        ref: "isVisibleOMP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "1M"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleIVM, 
                        ref: "isVisibleIVM", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "IV"
                ), 
                React.createElement("p", {style: style1}, 
                    "Round:", 
                    React.createElement("select", {
                        name: "round", 
                        ref: "roundAcc", 
                        id: "roundSelect", 
                        onChange: this.roundChange, 
                        defaultValue: "2"
                    }, 
                        React.createElement("option", {value: "0"}, "."), 
                        React.createElement("option", {value: "1"}, ".0"), 
                        React.createElement("option", {value: "2"}, ".00")
                    )
                )
            )
        );
    }
});
