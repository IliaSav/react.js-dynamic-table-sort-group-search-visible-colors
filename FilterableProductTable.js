/**
 * Created by admin on 5/7/15.
 */
var FilterableProductTable = React.createClass({displayName: "FilterableProductTable",
    getInitialState: function() {
        return {
            filterText: '',
            inStockOnly: false,
            visiblePr: true,
            visibleODP: true,
            visibleFDP: true,
            visibleOMP: true,
            visibleIVM: true,
            groupBy: 'sector',
            sortTicker: null,
            sortPr: null,
            sortODP: null,
            sortFDP: null,
            sortOMP: null,
            sortIVM:null,
            round: 2
        };
    },
    scroll: function(){
        var tables=document.getElementsByClassName("table");
        //console.log(tables);
        for(var i in tables)
            (tables[i]).scrollLeft((this).scrollLeft());
    },
    handleRound: function(setRound){
        //console.log(setRound);
        this.setState({
            round: setRound
        });
    },
    handleUserInput: function( visiblePr, visibleODP, visibleFDP, visibleOMP, visibleIVM, selectedGroup) {
        this.setState({
            visiblePr: visiblePr,
            visibleODP: visibleODP,
            visibleFDP: visibleFDP,
            visibleOMP: visibleOMP,
            visibleIVM: visibleIVM,
            groupBy: selectedGroup
        });
    },
    handleSortFake: function(state){
        var states={
            sortTickerVar: this.state.sortTicker,
            sortPrVar: this.state.sortPr,
            sortODPVar: this.state.sortODP,
            sortFDPVar: this.state.sortFDP,
            sortOMPVar: this.state.sortOMP,
            sortIVMVar: this.state.sortIVM
        };
        var change = function(val){
            var result=null;
            if(val==null) result=true;
            else if(val==true) result=false;
            return result;
        };
        states[state]=change(states[state]);
        //alert(JSON.stringify(states));
        this.setState({
            sortTicker: states["sortTickerVar"],
            sortPr: states["sortPrVar"],
            sortODP: states["sortODPVar"],
            sortFDP: states["sortFDPVar"],
            sortOMP: states["sortOMPVar"],
            sortIVM: states["sortIVMVar"]
        });
    },
    handleSort: function(sortTicker, sortPr, sortODP, sortFDP, sortOMP, sortIVM){
        if(sortTicker!==this.state.sortTicker){
            var sortTickerVar = null;
            if(sortTicker==null) sortTickerVar=true;
            else if(sortTicker==true) sortTickerVar=false;
        }
        var sortPrVar = null;
        if(sortPr==null) sortPrVar=true;
        else if(sortPr==true) sortPrVar=false;
        var sortODPVar = null;
        if(sortODP==null) sortODPVar=true;
        else if(sortODP==true) sortODPVar=false;
        var sortFDPVar = null;
        if(sortFDP==null) sortFDPVar=true;
        else if(sortFDP==true) sortFDPVar=false;
        var sortOMPVar = null;
        if(sortOMP==null) sortOMPVar=true;
        else if(sortOMP==true) sortOMPVar=false;
        var sortIVMVar = null;
        if(sortIVM==null) sortIVMVar=true;
        else if(sortIVM==true) sortIVMVar=false;
        //alert("Sort\nTicker:"+sortTicker+".."+sortTickerVar+" Pr:"+sortPr+".."+sortPrVar+" 1D%:"+sortODPVar+" 5D%:"+sortFDPVar);
        this.setState({
            sortTicker: sortTickerVar,
            sortPr: sortPrVar,
            sortODP: sortODPVar,
            sortFDP: sortFDPVar,
            sortOMP: sortOMPVar,
            sortIVM: sortIVMVar
        });
    },
    render: function() {
        console.time("main");
        var rows = [];
        var sort_par = [];
        var sort_order=[];
        /*
        var groupBy = this.state.groupBy;
        var sortTickerCr = (this.state.sortTicker===null) ? null : 'ticker';
        var sortPrCr = (this.state.sortPr===null) ? null : 'previous_close';
        var sortODPCr = (this.state.sortODP===null) ? null : 'one_day_percent';
        var sortFDPCr = (this.state.sortFDP===null) ? null : 'five_days_percent';
        var sortOMPCr = (this.state.sortOMP===null) ? null : 'one_month_percent';
        var sortIVMCr = (this.state.sortIVM===null) ? null : 'ivm';
        */
        sort_par.push(this.state.groupBy);
        sort_order.push(true);
        if(this.state.sortTicker!==null){
            sort_par.push("ticker");
            sort_order.push(this.state.sortTicker);
        }
        if(this.state.sortPr!==null){
            sort_par.push("previous_close");
            sort_order.push(this.state.sortPr);
        }
        if(this.state.sortODP!==null){
            sort_par.push("one_day_percent");
            sort_order.push(this.state.sortODP);
        }
        if(this.state.sortFDP!==null){
            sort_par.push("five_days_percent");
            sort_order.push(this.state.sortFDP);
        }
        if(this.state.sortOMP!==null){
            sort_par.push("one_month_percent");
            sort_order.push(this.state.sortOMP);
        }
        if(this.state.sortIVM!==null){
            sort_par.push("ivm");
            sort_order.push(this.state.sortIVM);
        }
        //alert("Sort\nTicker:"+this.state.sortTicker+" Pr:"+this.state.sortPr+" 1D%:"+this.state.sortODP+" 5D%:"+this.state.sortFDP);
        console.time("sort");
        var pr1 = _(this.props.products).sortByOrder(
            [groupBy,sortTickerCr, sortPrCr, sortODPCr, sortFDPCr,sortOMPCr,sortIVMCr],
            [true,this.state.sortTicker, this.state.sortPr, this.state.sortODP, this.state.sortFDP,this.state.sortOMP,this.state.sortIVM])
            .value();
        //var pr1=_(this.props.products).sortByOrder(sort_par,sort_order);
        console.timeEnd("sort");
        //var pr1 =this.props.products;
        //alert(pr1.length);
        var sizeArr = pr1.length;
        var height = window.screen.height-100;
        var delta = Math.round(height/20);
        var x1= 0, x2=x1+delta, n,dn;
        var dataArr = null;

        //alert("x2="+x2+"size"+sizeArr);
        while(x1<sizeArr){
            n=1;//element number
            dn=1;//row number
            while(dn<delta && x1+dn<sizeArr){
                if(pr1[x1+n][groupBy]!==pr1[x1+n-1][groupBy]) dn+=2;
                else dn++;
                //alert(JSON.stringify(pr1[x1+n-1][groupBy]));
                n++;
            }
            x2=x1+n;
            //alert(x2);
            dataArr =pr1.slice(x1,x2);
            //console.log(JSON.stringify(Object.keys(_(dataArr).groupBy(groupBy))));
            //alert(Object.keys(_(dataArr).groupBy(groupBy).__chain__));
            //console.log("x1="+x1+"..."+Object.keys(_(dataArr).groupBy(groupBy)).length);
            rows.push(
                React.createElement(ProductTable, {
                    products: dataArr, 
                    filterText: this.state.filterText, 
                    inStockOnly: this.state.inStockOnly, 
                    visiblePr: this.state.visiblePr, 
                    visibleODP: this.state.visibleODP, 
                    visibleFDP: this.state.visibleFDP, 
                    visibleOMP: this.state.visibleOMP, 
                    visibleIVM: this.state.visibleIVM, 
                    groupBy: this.state.groupBy, 
                    sortTicker: this.state.sortTicker, 
                    sortPr: this.state.sortPr, 
                    sortODP: this.state.sortODP, 
                    sortFDP: this.state.sortFDP, 
                    sortOMP: this.state.sortOMP, 
                    sortIVM: this.state.sortIVM, 
                    round: this.state.round, 
                    handleSort: this.handleSort, 
                    handleSortFake: this.handleSortFake, 
                    scroll: this.scroll}
                )
            );
            x1=x2;
            //x2+=delta;
        }
        console.timeEnd("main");
        return (
            React.createElement("div", null, 
                React.createElement(SearchBar, {
                    filterText: this.state.filterText, 
                    inStockOnly: this.state.inStockOnly, 
                    visiblePr: this.state.visiblePr, 
                    visibleODP: this.state.visibleODP, 
                    visibleFDP: this.state.visibleFDP, 
                    visibleOMP: this.state.visibleOMP, 
                    visibleIVM: this.state.visibleIVM, 
                    groupBy: this.state.groupBy, 
                    onUserInput: this.handleUserInput, 
                    onUserRoundChange: this.handleRound}
                ), 
                    rows
            )
        );
    }
});
