/**
 * Created by admin on 5/7/15.
 */
var ProductCategoryRow = React.createClass({displayName: "ProductCategoryRow",
    render: function() {
        var style={
            textAlign: 'left',
            backgroundColor: 'grey',
            color: 'black',
            fontWeight: 'bold'
        };
        var category = (this.props.category!==null) ? this.props.category.toUpperCase() : "";
        return (React.createElement("tr", null, React.createElement("th", {colSpan: "10", style: style}, category)));
    }
});

/**
 * Created by admin on 5/7/15.
 */
var ProductRow = React.createClass({displayName: "ProductRow",
    render: function() {
        var function_green_red = function(i){
            var elem = null;
            if(i>0){
                    elem =React.createElement("span", {style: {color: '#2ca02c'}}, 
                            i
                            );
            }
            else{
                    elem=React.createElement("span", {style: {color: '#a90a08'}}, 
                            i
                        );
            }
            return elem;
        };
        var pr =
            React.createElement("span", {style: {color: 'yellow'}}, 
                parseFloat(this.props.product.previous_close).toFixed(this.props.round)
            );
        var one_day_percent = function_green_red(parseFloat(this.props.product.one_day_percent).toFixed(this.props.round));
        var five_days_percent = function_green_red(parseFloat(this.props.product.five_days_percent).toFixed(this.props.round));
        var one_month_percent = function_green_red(parseFloat(this.props.product.one_month_percent).toFixed(this.props.round));

            var ivm =
                parseFloat(this.props.product.ivm).toFixed(this.props.round);
         /*
         * Set visibality of column
         */

        var show_pr = this.props.show_Pr ? pr : null;
        var show_ODP = this.props.show_ODP ? one_day_percent : null;
        var show_FDP = this.props.show_FDP ? five_days_percent : null;
        var show_OMP = this.props.show_OMP ? one_month_percent : null;
        var show_IVM = this.props.show_IVM ? ivm : null;
        /*
        * Return element
        */

        var style={
            borderRightColor: 'grey',
            borderRightStyle: 'dashed',
            borderRightWidth: 1,
            textAlign: 'left',
            color: '#d58512'
        }
        var style_numb={
            textAlign: 'right',
            paddingLeft: 5
        };

        var style_ivm={
            color: '#5BC0DE'
        };
        return (
            React.createElement("tr", {style: style_numb}, 
                React.createElement("td", {style: style}, this.props.product.ticker), 
                React.createElement("td", {className: "Pr"}, show_pr), 
                React.createElement("td", null, show_ODP), 
                React.createElement("td", null, show_FDP), 
                React.createElement("td", null, show_OMP), 
                React.createElement("td", {style: style_ivm}, show_IVM)
            )
        );
    }
});

/**
 * Created by admin on 5/8/15.
 */
var Header = React.createClass({displayName: "Header",
    fake: function(name){
        this.props.onUserSortFake(
            name
        )
    },
    sort: function(){
        //alert("Func Sort.."+this.refs.sortTickerRef.getDOMNode().value+".."+this.refs.sortPrRef.getDOMNode().value);
        this.props.onUserSort(
            this.refs.sortTickerRef.getDOMNode().value,
            this.refs.sortPrRef.getDOMNode().value,
            this.refs.sortODPRef.getDOMNode().value,
            this.refs.sortFDPRef.getDOMNode().value,
            this.refs.sortOMPRef.getDOMNode().value,
            this.refs.sortIVMRef.getDOMNode().value
        )
    },
    render: function() {
        /*var char = '';
        if (this.props.sortRand!==null){
            char = this.props.sortRand ? '\u25BC' : '\u25B2'
        }
        var rand = this.props.showRand ? "Rand"+char : null;
        */
        //alert("props.Ticker:"+this.props.sortTicker);
        var charTicker = '';
        if (this.props.sortTicker!==null){
            charTicker = this.props.sortTicker ? '\u25BC' : '\u25B2'
        }
        var Ticker = "Ticker"+charTicker;
        /****************************************************/
        var charPr = '';
        if (this.props.sortPr!==null){
            charPr = this.props.sortPr ? '\u25BC' : '\u25B2'
        }
        var Pr = this.props.show_Pr ? "Pr"+charPr : null;
        /****************************************************/
        var charODP = '';
        if (this.props.sortODP!==null){
            charODP = this.props.sortODP ? '\u25BC' : '\u25B2'
        }
        var ODP = this.props.show_ODP ? "1D"+charODP : null;
        /****************************************************/
        var charFDP = '';
        if (this.props.sortFDP!==null){
            charFDP = this.props.sortFDP ? '\u25BC' : '\u25B2'
        }
        var FDP = this.props.show_FDP ? "5D"+charFDP : null;
        /****************************************************/
        var charOMP = '';
        if (this.props.sortOMP!==null){
            charOMP = this.props.sortOMP ? '\u25BC' : '\u25B2'
        }
        var OMP = this.props.show_OMP ? "1M"+charOMP : null;
        /****************************************************/
        var charIVM = '';
        if (this.props.sortIVM!==null){
            charIVM = this.props.sortIVM ? '\u25BC' : '\u25B2'
        }
        var IVM = this.props.show_IVM ? "IV"+charIVM : null;
        var style={
            borderRightColor: 'grey',
            borderRightStyle: 'dashed',
            borderRightWidth: 1,
            cursor: 'pointer'
            };
        var headerStyle={
            cursor: 'pointer'
        };
        return (
            React.createElement("thead", null, 
                React.createElement("tr", {style: headerStyle}, 
                    React.createElement("th", {
                        value: this.props.sortTicker, 
                        onClick: this.fake.bind(this,"sorTickerVar"), 
                        ref: "sortTickerRef", 
                        style: style
                    }, Ticker), 
                    React.createElement("th", {
                        value: this.props.sortPr, 
                        onClick: this.fake.bind(this,"sortPrVar"), 
                        ref: "sortPrRef"
                    }, Pr), 
                    React.createElement("th", {
                        value: this.props.sortODP, 
                        onClick: this.fake.bind(this,"sortODPVar"), 
                        ref: "sortODPRef"
                    }, ODP), 
                    React.createElement("th", {
                        value: this.props.sortFDP, 
                        onClick: this.fake.bind(this,"sortFDPVar"), 
                        ref: "sortFDPRef"
                    }, FDP), 
                    React.createElement("th", {
                        value: this.props.sortOMP, 
                        onClick: this.fake.bind(this,"sortOMPVar"), 
                        ref: "sortOMPRef"
                    }, OMP), 
                    React.createElement("th", {
                        value: this.props.sortIVM, 
                        onClick: this.fake.bind(this,"sortIVMVar"), 
                        ref: "sortIVMRef"
                    }, IVM)

                )
            )
        );
    }
});

/**
 * Created by admin on 5/7/15.
 */
var ProductTable = React.createClass({displayName: "ProductTable",
    scroll: function(){
        var tables=document.getElementsByClassName("table");
        //console.log(tables);
        //console.log(this.refs.tb.getDOMNode().scrollLeft);
        var scroll_y=this.refs.tb.getDOMNode().scrollLeft;
        for(var i in tables)
            tables[i].scrollLeft=scroll_y;
    },
    nothing: function(){
        return;
    },
    resizeTable: function(event){
        var tb=this.refs.tb.getDOMNode();
        var lastX;
        console.log("mouseDown" + event.pageX);
        //if (event.which == 1) {
            lastX = event.pageX;
            addEventListener("mousemove", moved);
            //if(tb.onmousemove) moved(event);
            event.preventDefault(); // Prevent selection
        //}
        function buttonPressed(event) {
            if (event.buttons == null)
                return event.which != 0;
            else
                return event.buttons != 0;
            }

        function moved(event) {
            console.log("moved");
            if (!buttonPressed(event)) {
                removeEventListener("mousemove", moved);
                var tables=document.getElementsByClassName("table");
                for(var i in tables)
                    tables[i].style.width = tb.style.width;
          } else {
            var dist = event.pageX - lastX;
            var newWidth = Math.max(10, tb.offsetWidth + dist);
            tb.style.width = newWidth + "px";
            //var tables=document.getElementsByClassName("table");
            //    for(var i in tables)
            //    tables[i].style.width = newWidth + "px";
            lastX = event.pageX;
          }
  }
    },
    render: function() {
        var groupBy = this.props.groupBy;
        var rows = [];
        var lastCategory = null;
        this.props.products.forEach(function(product) {
            /*if (product.name.indexOf(this.props.filterText) === -1 || (!product.stocked && this.props.inStockOnly)) {
                return;
            }*/
            if (product[groupBy] !== lastCategory) {
                rows.push(React.createElement(ProductCategoryRow, {category: product[groupBy], key: product[groupBy]}));
            }
            rows.push(
                React.createElement(ProductRow, {
                    product: product, 
                    key: product.ticker, 
                    show_Pr: this.props.visiblePr, 
                    show_ODP: this.props.visibleODP, 
                    show_FDP: this.props.visibleFDP, 
                    show_OMP: this.props.visibleOMP, 
                    show_IVM: this.props.visibleIVM, 
                    round: this.props.round}
                )
            );
            lastCategory = product[groupBy];
        }.bind(this));
        var style = {
            display: 'inline-block',
            marginRight: 0,
            outlineWidth: 1,
            outlineColor: 'grey',
            outlineStyle: 'solid ',
            position: 'relative',
            backgroundColor: 'black',
            top: 0,
            width: 'auto',
            overflowX: 'auto',
            fontSize: 11,
            verticalAlign: 'top',
            height: window.screen.height-100
        };
        var inline = {
            display: 'inline-block'
        };
        var resize ={
            display: 'inline-block',
            borderLeftColor: 'grey',
            borderLeftStyle: 'solid',
            borderLeftWidth: 1,
            marginRight: 5,
            height: window.screen.height-270,
            verticalAlign: 'top',
            float: 'none',
            width: 2,
            cursor: 'ew-resize'
        };

        return (
           React.createElement("div", {
               style: inline
           }, 
            React.createElement("table", {
                style: style, 
                onScroll: this.scroll, 
                className: "table", 
                ref: "tb"
            }, 
                React.createElement(Header, {
                    show_Pr: this.props.visiblePr, 
                    show_ODP: this.props.visibleODP, 
                    show_FDP: this.props.visibleFDP, 
                    show_OMP: this.props.visibleOMP, 
                    show_IVM: this.props.visibleIVM, 
                    sortTicker: this.props.sortTicker, 
                    sortPr: this.props.sortPr, 
                    sortODP: this.props.sortODP, 
                    sortFDP: this.props.sortFDP, 
                    sortOMP: this.props.sortOMP, 
                    sortIVM: this.props.sortIVM, 
                    onUserSort: this.props.handleSort, 
                    onUserSortFake: this.props.handleSortFake}
                ), 
                React.createElement("tbody", null, rows)
            ), 
            React.createElement("div", {
                style: resize, 
                onMouseDown: this.resizeTable
            }
                )
          )
        );
    }
});

/**
 * Created by admin on 5/7/15.
 */
var SearchBar = React.createClass({displayName: "SearchBar",
    handleChange: function() {
        this.props.onUserInput(
            //this.refs.filterTextInput.getDOMNode().value,
            //this.refs.inStockOnlyInput.getDOMNode().checked,
            this.refs.isVisiblePr.getDOMNode().checked,
            this.refs.isVisibleODP.getDOMNode().checked,
            this.refs.isVisibleFDP.getDOMNode().checked,
            this.refs.isVisibleOMP.getDOMNode().checked,
            this.refs.isVisibleIVM.getDOMNode().checked,
            this.refs.groupSelected.getDOMNode().value
        );
    },
    roundChange: function(){
        this.props.onUserRoundChange(
            this.refs.roundAcc.getDOMNode().value
        );
    },
    render: function() {
        var style = {
            display: 'inline-block',
            paddingLeft: 3
            };
        var style1 = {
            display: 'inline-block',
            paddingLeft: 15
            };
        return (
React.createElement("form", null, 
                React.createElement("p", {style: style1}, 
                    "Group By:", 
                    React.createElement("select", {
                        name: "groupBy", 
                        ref: "groupSelected", 
                        id: "categorySelect", 
                        onChange: this.handleChange
                    }, 
                        React.createElement("option", {value: "sector"}, "Sector"), 
                        React.createElement("option", {value: "industry"}, "Industry"), 
                        React.createElement("option", {value: "country"}, "Country")
                    )
                ), 
                React.createElement("p", {style: style1}, 
                    "Show columns:", 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visiblePr, 
                        ref: "isVisiblePr", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "Pr"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleODP, 
                        ref: "isVisibleODP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "1D"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleFDP, 
                        ref: "isVisibleFDP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "5D"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleOMP, 
                        ref: "isVisibleOMP", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "1M"
                ), 
                React.createElement("p", {style: style}, 
                    React.createElement("input", {
                        type: "checkbox", 
                        checked: this.props.visibleIVM, 
                        ref: "isVisibleIVM", 
                        onChange: this.handleChange}
                    ), 
                    ' ', 
                    "IV"
                ), 
                React.createElement("p", {style: style1}, 
                    "Round:", 
                    React.createElement("select", {
                        name: "round", 
                        ref: "roundAcc", 
                        id: "roundSelect", 
                        onChange: this.roundChange
                    }, 
                        React.createElement("option", {value: "0"}, "."), 
                        React.createElement("option", {value: "1"}, ".0"), 
                        React.createElement("option", {selected: true, value: "2"}, ".00")
                    )
                )
            )
        );
    }
});

/**
 * Created by admin on 5/7/15.
 */
var FilterableProductTable = React.createClass({displayName: "FilterableProductTable",
    getInitialState: function() {
        return {
            filterText: '',
            inStockOnly: false,
            visiblePr: true,
            visibleODP: true,
            visibleFDP: true,
            visibleOMP: true,
            visibleIVM: true,
            groupBy: 'sector',
            sortTicker: null,
            sortPr: null,
            sortODP: null,
            sortFDP: null,
            sortOMP: null,
            sortIVM:null,
            round: 2
        };
    },
    scroll: function(){
        var tables=document.getElementsByClassName("table");
        //console.log(tables);
        for(var i in tables)
            (tables[i]).scrollLeft((this).scrollLeft());
    },
    handleRound: function(setRound){
        //console.log(setRound);
        this.setState({
            round: setRound
        });
    },
    handleUserInput: function( visiblePr, visibleODP, visibleFDP, visibleOMP, visibleIVM, selectedGroup) {
        this.setState({
            visiblePr: visiblePr,
            visibleODP: visibleODP,
            visibleFDP: visibleFDP,
            visibleOMP: visibleOMP,
            visibleIVM: visibleIVM,
            groupBy: selectedGroup
        });
    },
    handleSortFake: function(state){
        var states={
            sortTickerVar: this.state.sortTicker,
            sortPrVar: this.state.sortPr,
            sortODPVar: this.state.sortODP,
            sortFDPVar: this.state.sortFDP,
            sortOMPVar: this.state.sortOMP,
            sortIVMVar: this.state.sortIVM
        };
        var change = function(val){
            var result=null;
            if(val==null) result=true;
            else if(val==true) result=false;
            return result;
        };
        states[state]=change(states[state]);
        //alert(JSON.stringify(states));
        this.setState({
            sortTicker: states["sortTickerVar"],
            sortPr: states["sortPrVar"],
            sortODP: states["sortODPVar"],
            sortFDP: states["sortFDPVar"],
            sortOMP: states["sortOMPVar"],
            sortIVM: states["sortIVMVar"]
        });
    },
    handleSort: function(sortTicker, sortPr, sortODP, sortFDP, sortOMP, sortIVM){
        if(sortTicker!==this.state.sortTicker){
            var sortTickerVar = null;
            if(sortTicker==null) sortTickerVar=true;
            else if(sortTicker==true) sortTickerVar=false;
        }
        var sortPrVar = null;
        if(sortPr==null) sortPrVar=true;
        else if(sortPr==true) sortPrVar=false;
        var sortODPVar = null;
        if(sortODP==null) sortODPVar=true;
        else if(sortODP==true) sortODPVar=false;
        var sortFDPVar = null;
        if(sortFDP==null) sortFDPVar=true;
        else if(sortFDP==true) sortFDPVar=false;
        var sortOMPVar = null;
        if(sortOMP==null) sortOMPVar=true;
        else if(sortOMP==true) sortOMPVar=false;
        var sortIVMVar = null;
        if(sortIVM==null) sortIVMVar=true;
        else if(sortIVM==true) sortIVMVar=false;
        this.setState({
            sortTicker: sortTickerVar,
            sortPr: sortPrVar,
            sortODP: sortODPVar,
            sortFDP: sortFDPVar,
            sortOMP: sortOMPVar,
            sortIVM: sortIVMVar
        });
    },
    render: function() {
        console.time("main");
        var rows = [];
        var sort_par = [];
        var sort_order=[];
        var groupBy = this.state.groupBy;
        /*var sortTickerCr = (this.state.sortTicker===null) ? null : 'ticker';
        var sortPrCr = (this.state.sortPr===null) ? null : 'previous_close';
        var sortODPCr = (this.state.sortODP===null) ? null : 'one_day_percent';
        var sortFDPCr = (this.state.sortFDP===null) ? null : 'five_days_percent';
        var sortOMPCr = (this.state.sortOMP===null) ? null : 'one_month_percent';
        var sortIVMCr = (this.state.sortIVM===null) ? null : 'ivm';
        */
        sort_par.push(this.state.groupBy);
        sort_order.push("true");
        if(this.state.sortTicker!==null){
            sort_par.push("ticker");
            sort_order.push(this.state.sortTicker);
        }
        if(this.state.sortPr!==null){
            sort_par.push("previous_close");
            sort_order.push(this.state.sortPr);
        }
        if(this.state.sortODP!==null){
            sort_par.push("one_day_percent");
            sort_order.push(this.state.sortODP);
        }
        if(this.state.sortFDP!==null){
            sort_par.push("five_days_percent");
            sort_order.push(this.state.sortFDP);
        }
        if(this.state.sortOMP!==null){
            sort_par.push("one_month_percent");
            sort_order.push(this.state.sortOMP);
        }
        if(this.state.sortIVM!==null){
            sort_par.push("ivm");
            sort_order.push(this.state.sortIVM);
        }
        //alert("Sort\nTicker:"+this.state.sortTicker+" Pr:"+this.state.sortPr+" 1D%:"+this.state.sortODP+" 5D%:"+this.state.sortFDP);
        console.time("sort");
        /*var pr1 = _(this.props.products).sortByOrder(
            [groupBy,sortTickerCr, sortPrCr, sortODPCr, sortFDPCr,sortOMPCr,sortIVMCr],
            [true,this.state.sortTicker, this.state.sortPr, this.state.sortODP, this.state.sortFDP,this.state.sortOMP,this.state.sortIVM])
            .value();*/
        var pr1=_(this.props.products).sortByOrder(sort_par,sort_order).value();
        console.timeEnd("sort");
        //alert(pr1.length);
        //console.log(JSON.stringify(pr1));
        var sizeArr = pr1.length;
        var height = window.screen.height-100;
        var delta = Math.round(height/20);
        var x1= 0, x2=x1+delta, n,dn;
        var dataArr = null;

        //alert("x2="+x2+"size"+sizeArr);
        while(x1<sizeArr){
            n=1;//element number
            dn=1;//row number
            while(dn<delta && x1+dn<sizeArr){
                if(pr1[x1+n][groupBy]!==pr1[x1+n-1][groupBy]) dn+=2;
                else dn++;
                //alert(JSON.stringify(pr1[x1+n-1][groupBy]));
                n++;
            }
            x2=x1+n;
            //alert(x2);
            dataArr =pr1.slice(x1,x2);
            //console.log(JSON.stringify(Object.keys(_(dataArr).groupBy(groupBy))));
            //alert(Object.keys(_(dataArr).groupBy(groupBy).__chain__));
            //console.log("x1="+x1+"..."+Object.keys(_(dataArr).groupBy(groupBy)).length);
            rows.push(
                React.createElement(ProductTable, {
                    products: dataArr, 
                    filterText: this.state.filterText, 
                    inStockOnly: this.state.inStockOnly, 
                    visiblePr: this.state.visiblePr, 
                    visibleODP: this.state.visibleODP, 
                    visibleFDP: this.state.visibleFDP, 
                    visibleOMP: this.state.visibleOMP, 
                    visibleIVM: this.state.visibleIVM, 
                    groupBy: this.state.groupBy, 
                    sortTicker: this.state.sortTicker, 
                    sortPr: this.state.sortPr, 
                    sortODP: this.state.sortODP, 
                    sortFDP: this.state.sortFDP, 
                    sortOMP: this.state.sortOMP, 
                    sortIVM: this.state.sortIVM, 
                    round: this.state.round, 
                    handleSort: this.handleSort, 
                    handleSortFake: this.handleSortFake, 
                    scroll: this.scroll}
                )
            );
            x1=x2;
            //x2+=delta;
        }
        console.timeEnd("main");
        return (
            React.createElement("div", null, 
                React.createElement(SearchBar, {
                    filterText: this.state.filterText, 
                    inStockOnly: this.state.inStockOnly, 
                    visiblePr: this.state.visiblePr, 
                    visibleODP: this.state.visibleODP, 
                    visibleFDP: this.state.visibleFDP, 
                    visibleOMP: this.state.visibleOMP, 
                    visibleIVM: this.state.visibleIVM, 
                    groupBy: this.state.groupBy, 
                    onUserInput: this.handleUserInput, 
                    onUserRoundChange: this.handleRound}
                ), 
                    rows
            )
        );
    }
});

$.get('/api/universe', {}, function(data) {
    var DATA1=[];
    data.forEach(function(data){
        var d={};
        d["ticker"]=data["ticker"];
        d["name"]=data["name"];
        d["sector"]=(data["sector"]===null) ? "Unknown" : data["sector"];
        d["industry"]=(data["industry"]===null) ? "Unknown" : data["industry"];
        d["country"]=data["country"];
        d["market_capitalization"]=Number(data["market_capitalization"]);
        d["last_trade_price"]=Number(data["last_trade_price"]);
        d["previous_close"]=Number(data["previous_close"]);
        d["one_day_percent"]=Number(data["one_day_percent"]);
        d["five_days_percent"]=Number(data["five_days_percent"]);
        d["one_month_percent"]=Number(data["one_month_percent"]);
        d["ivm"]=Number(data["ivm"]);
        d["fmx_days"]=Number(data["fmx_days"]);
        d["fmn_days"]=Number(data["fmn_days"]);
        DATA1.push(d);
    });

    React.render(React.createElement(FilterableProductTable, {products: DATA1}), document.getElementById('table'));
}, 'json');