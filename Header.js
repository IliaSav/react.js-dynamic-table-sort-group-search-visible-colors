/**
 * Created by admin on 5/8/15.
 */
var Header = React.createClass({displayName: "Header",
    fake: function(name){
        this.props.onUserSortFake(
            name
        )
    },
    sort: function(){
        //alert("Func Sort.."+this.refs.sortTickerRef.getDOMNode().value+".."+this.refs.sortPrRef.getDOMNode().value);
        this.props.onUserSort(
            this.refs.sortTickerRef.getDOMNode().value,
            this.refs.sortPrRef.getDOMNode().value,
            this.refs.sortODPRef.getDOMNode().value,
            this.refs.sortFDPRef.getDOMNode().value,
            this.refs.sortOMPRef.getDOMNode().value,
            this.refs.sortIVMRef.getDOMNode().value
        )
    },
    render: function() {
        /*var char = '';
        if (this.props.sortRand!==null){
            char = this.props.sortRand ? '\u25BC' : '\u25B2'
        }
        var rand = this.props.showRand ? "Rand"+char : null;
        */
        //alert("props.Ticker:"+this.props.sortTicker);
        var charTicker = '';
        if (this.props.sortTicker!==null){
            charTicker = this.props.sortTicker ? '\u25BC' : '\u25B2'
        }
        var Ticker = "Ticker"+charTicker;
        /****************************************************/
        var charPr = '';
        if (this.props.sortPr!==null){
            charPr = this.props.sortPr ? '\u25BC' : '\u25B2'
        }
        var Pr = this.props.show_Pr ? "Pr"+charPr : null;
        /****************************************************/
        var charODP = '';
        if (this.props.sortODP!==null){
            charODP = this.props.sortODP ? '\u25BC' : '\u25B2'
        }
        var ODP = this.props.show_ODP ? "1D"+charODP : null;
        /****************************************************/
        var charFDP = '';
        if (this.props.sortFDP!==null){
            charFDP = this.props.sortFDP ? '\u25BC' : '\u25B2'
        }
        var FDP = this.props.show_FDP ? "5D"+charFDP : null;
        /****************************************************/
        var charOMP = '';
        if (this.props.sortOMP!==null){
            charOMP = this.props.sortOMP ? '\u25BC' : '\u25B2'
        }
        var OMP = this.props.show_OMP ? "1M"+charOMP : null;
        /****************************************************/
        var charIVM = '';
        if (this.props.sortIVM!==null){
            charIVM = this.props.sortIVM ? '\u25BC' : '\u25B2'
        }
        var IVM = this.props.show_IVM ? "IV"+charIVM : null;
        var style={
            borderRightColor: 'grey',
            borderRightStyle: 'dashed',
            borderRightWidth: 1,
            cursor: 'pointer'
            };
        var headerStyle={
            cursor: 'pointer'
        };
        return (
            React.createElement("thead", null, 
                React.createElement("tr", {style: headerStyle}, 
                    React.createElement("th", {
                        value: this.props.sortTicker, 
                        onClick: this.fake.bind(this,"sorTickerVar"), 
                        ref: "sortTickerRef", 
                        style: style
                    }, Ticker), 
                    React.createElement("th", {
                        value: this.props.sortPr, 
                        onClick: this.fake.bind(this,"sortPrVar"), 
                        ref: "sortPrRef"
                    }, Pr), 
                    React.createElement("th", {
                        value: this.props.sortODP, 
                        onClick: this.fake.bind(this,"sortODPVar"), 
                        ref: "sortODPRef"
                    }, ODP), 
                    React.createElement("th", {
                        value: this.props.sortFDP, 
                        onClick: this.fake.bind(this,"sortFDPVar"), 
                        ref: "sortFDPRef"
                    }, FDP), 
                    React.createElement("th", {
                        value: this.props.sortOMP, 
                        onClick: this.fake.bind(this,"sortOMPVar"), 
                        ref: "sortOMPRef"
                    }, OMP), 
                    React.createElement("th", {
                        value: this.props.sortIVM, 
                        onClick: this.fake.bind(this,"sortIVMVar"), 
                        ref: "sortIVMRef"
                    }, IVM)

                )
            )
        );
    }
});