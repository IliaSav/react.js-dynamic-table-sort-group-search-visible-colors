/**
 * Created by admin on 5/7/15.
 */
var ProductRow = React.createClass({displayName: "ProductRow",
    render: function() {
        var function_green_red = function(i){
            var elem = null;
            if(i>0){
                    elem =React.createElement("span", {style: {color: '#2ca02c'}}, 
                            i
                            );
            }
            else{
                    elem=React.createElement("span", {style: {color: '#a90a08'}}, 
                            i
                        );
            }
            return elem;
        };
        var pr =
            React.createElement("span", {style: {color: 'yellow'}}, 
                parseFloat(this.props.product.previous_close).toFixed(this.props.round)
            );
        var one_day_percent = function_green_red(parseFloat(this.props.product.one_day_percent).toFixed(this.props.round));
        var five_days_percent = function_green_red(parseFloat(this.props.product.five_days_percent).toFixed(this.props.round));
        var one_month_percent = function_green_red(parseFloat(this.props.product.one_month_percent).toFixed(this.props.round));

            var ivm =
                parseFloat(this.props.product.ivm).toFixed(this.props.round);
         /*
         * Set visibality of column
         */

        var show_pr = this.props.show_Pr ? pr : null;
        var show_ODP = this.props.show_ODP ? one_day_percent : null;
        var show_FDP = this.props.show_FDP ? five_days_percent : null;
        var show_OMP = this.props.show_OMP ? one_month_percent : null;
        var show_IVM = this.props.show_IVM ? ivm : null;
        /*
        * Return element
        */

        var style={
            borderRightColor: 'grey',
            borderRightStyle: 'dashed',
            borderRightWidth: 1,
            textAlign: 'left',
            color: '#d58512'
        }
        var style_numb={
            textAlign: 'right',
            paddingLeft: 5
        };

        var style_ivm={
            color: '#5BC0DE'
        };
        return (
            React.createElement("tr", {style: style_numb}, 
                React.createElement("td", {style: style, 
                    className: "tdTicker"}, this.props.product.ticker), 
                React.createElement("td", {className: "Pr"}, show_pr), 
                React.createElement("td", null, show_ODP), 
                React.createElement("td", null, show_FDP), 
                React.createElement("td", null, show_OMP), 
                React.createElement("td", {style: style_ivm}, show_IVM)
            )
        );
    }
});