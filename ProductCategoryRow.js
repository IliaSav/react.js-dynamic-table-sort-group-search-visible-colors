/**
 * Created by admin on 5/7/15.
 */
var ProductCategoryRow = React.createClass({displayName: "ProductCategoryRow",
    render: function() {
        var style={
            textAlign: 'left',
            backgroundColor: 'grey',
            color: 'black',
            fontWeight: 'bold'
        };
        var category = (this.props.category!==null) ? this.props.category.toUpperCase() : "";
        return (React.createElement("tr", null, React.createElement("th", {colSpan: "10", style: style}, category)));
    }
});