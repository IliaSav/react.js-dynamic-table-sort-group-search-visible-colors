/**
 * Created by admin on 5/7/15.
 */
var ProductTable = React.createClass({displayName: "ProductTable",
    scroll: function(){
        var tables=document.getElementsByClassName("table");
        //console.log(tables);
        //console.log(this.refs.tb.getDOMNode().scrollLeft);
        var scroll_y=this.refs.tb.getDOMNode().scrollLeft;
        for(var i in tables)
            tables[i].scrollLeft=scroll_y;
    },
    nothing: function(){
        return;
    },
    resizeTable: function(event){
        var tb=this.refs.tb.getDOMNode();
        var lastX;
        console.log("mouseDown" + event.pageX);
        //if (event.which == 1) {
            lastX = event.pageX;
            addEventListener("mousemove", moved);
            //if(tb.onmousemove) moved(event);
            event.preventDefault(); // Prevent selection
        //}
        function buttonPressed(event) {
            if (event.buttons == null)
                return event.which != 0;
            else
                return event.buttons != 0;
            }

        function moved(event) {
            console.log("moved");
            if (!buttonPressed(event)) {
                removeEventListener("mousemove", moved);
                var tables=document.getElementsByClassName("table");
                for(var i in tables)
                    tables[i].style.width = tb.style.width;
          } else {
            var dist = event.pageX - lastX;
            var newWidth = Math.max(10, tb.offsetWidth + dist);
            tb.style.width = newWidth + "px";
            //var tables=document.getElementsByClassName("table");
            //    for(var i in tables)
            //    tables[i].style.width = newWidth + "px";
            lastX = event.pageX;
          }
  }
    },
    render: function() {
        var groupBy = this.props.groupBy;
        var rows = [];
        var lastCategory = null;
        this.props.products.forEach(function(product) {
            /*if (product.name.indexOf(this.props.filterText) === -1 || (!product.stocked && this.props.inStockOnly)) {
                return;
            }*/
            if (product[groupBy] !== lastCategory) {
                rows.push(React.createElement(ProductCategoryRow, {category: product[groupBy], key: product[groupBy]}));
            }
            rows.push(
                React.createElement(ProductRow, {
                    product: product, 
                    key: product.ticker, 
                    show_Pr: this.props.visiblePr, 
                    show_ODP: this.props.visibleODP, 
                    show_FDP: this.props.visibleFDP, 
                    show_OMP: this.props.visibleOMP, 
                    show_IVM: this.props.visibleIVM, 
                    round: this.props.round}
                )
            );
            lastCategory = product[groupBy];
        }.bind(this));
        var style = {
            display: 'inline-block',
            marginRight: 0,
            outlineWidth: 1,
            outlineColor: 'grey',
            outlineStyle: 'solid ',
            position: 'relative',
            backgroundColor: 'black',
            top: 0,
            width: 'auto',
            overflowX: 'auto',
            fontSize: 11,
            verticalAlign: 'top'
        };
        var inline = {
            display: 'inline-block'
        };
        var resize ={
            display: 'inline-block',
            borderLeftColor: 'grey',
            borderLeftStyle: 'solid',
            borderLeftWidth: 1,
            marginRight: 5,
            height: window.screen.height-270,
            verticalAlign: 'top',
            float: 'none',
            width: 2,
            cursor: 'ew-resize'
        };

        return (
           React.createElement("div", {
               style: inline
           }, 
            React.createElement("table", {
                style: style, 
                onScroll: this.scroll, 
                className: "table", 
                ref: "tb"
            }, 
                React.createElement(Header, {
                    show_Pr: this.props.visiblePr, 
                    show_ODP: this.props.visibleODP, 
                    show_FDP: this.props.visibleFDP, 
                    show_OMP: this.props.visibleOMP, 
                    show_IVM: this.props.visibleIVM, 
                    sortTicker: this.props.sortTicker, 
                    sortPr: this.props.sortPr, 
                    sortODP: this.props.sortODP, 
                    sortFDP: this.props.sortFDP, 
                    sortOMP: this.props.sortOMP, 
                    sortIVM: this.props.sortIVM, 
                    onUserSort: this.props.handleSort, 
                    onUserSortFake: this.props.handleSortFake}
                ), 
                React.createElement("tbody", null, rows)
            ), 
            React.createElement("div", {
                style: resize, 
                onMouseDown: this.resizeTable
            }
                )
          )
        );
    }
});